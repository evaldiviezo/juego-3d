using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneradorObstaculos : MonoBehaviour
{
    public List<GameObject> obstaculoPrefabs; // Lista de prefabs de obstáculos
    public float tiempoEntreObstaculos = 2f; // Tiempo en segundos entre la generación de cada obstáculo
    public float alturaGeneracion = 10f; // Altura desde la cual se generarán los obstáculos
    public float distanciaDelanteJugador = 10f; // Distancia delante del jugador donde se generarán los obstáculos
    public float anchoGeneracion = 4f; // Ancho de la zona de generación de obstáculos en frente del jugador
    private Car jugador; // Referencia al script del jugador (asegúrate de que 'Car' es el componente correcto)
    private float tiempoPasado = 0f;

    void Start()
    {
        // Intenta encontrar el componente Car en el juego. Asegúrate de que el script del jugador esté correctamente asignado.
        jugador = FindObjectOfType<Car>();
        if (jugador == null)
        {
            Debug.LogError("¡No se encontró el script del jugador!");
        }
    }

    void Update()
    {
        tiempoPasado += Time.deltaTime;
        if (tiempoPasado >= tiempoEntreObstaculos)
        {
            GenerarObstaculo();
            tiempoPasado = 0f;
        }
    }

    void GenerarObstaculo()
    {
        if (obstaculoPrefabs.Count == 0)
        {
            Debug.LogError("¡Lista de prefabs de obstáculos vacía!");
            return;
        }

        // Obtener la posición actual del jugador
        Vector3 posicionJugador = jugador.transform.position;

        // La posición del generador será un poco adelante del jugador, ajustando la distancia en el eje Z
        Vector3 posicionGenerador = posicionJugador + Vector3.forward * distanciaDelanteJugador;

        // Elegir un prefab de obstáculo aleatorio de la lista
        GameObject obstaculoPrefab = obstaculoPrefabs[Random.Range(0, obstaculoPrefabs.Count)];

        // Generar un punto aleatorio dentro del ancho de generación, pero manteniéndolo siempre frente al jugador
        Vector3 posicionAleatoria = posicionGenerador + new Vector3(Random.Range(-anchoGeneracion, anchoGeneracion), alturaGeneracion, 0);

        // Finalmente, instanciar el obstáculo en la posición calculada
        Instantiate(obstaculoPrefab, posicionAleatoria, Quaternion.identity);
    }
}