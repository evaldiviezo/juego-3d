using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : MonoBehaviour
{
    /*
    public float velocidadHorizontal = 5f; // Velocidad de movimiento horizontal del jugador
    public float velocidadAdelante = 10f; // Velocidad de movimiento hacia adelante del jugador
    public float velocidadGiro = 100f; // Velocidad de giro del vehículo
    */
    public float velocidadAdelante = 5f;
    public float velocidadHorizontal = 2f;
    public float aceleracionGiro = 30f;
    private float anguloGiro = 0f;


    void Update()
    {

        // Movimiento hacia adelante automático
        transform.Translate(Vector3.forward * Time.deltaTime * velocidadAdelante);

        // Giro progresivo del vehículo
        if (Input.GetKey(KeyCode.A))
        {
            anguloGiro -= aceleracionGiro * Time.deltaTime;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            anguloGiro += aceleracionGiro * Time.deltaTime;
        }
        else
        {
            anguloGiro = 0; // Resetea el giro cuando no se presiona A o D
        }
        
        transform.Rotate(Vector3.up, anguloGiro * Time.deltaTime);
    }
}
